export interface FakeResponse {
    type?: string;
    url?: string;
    redirected?: boolean;
    status?: number;
    ok?: boolean;
    statusText?: string;
    headers?: object;
    body?: ReadableStream;
    bodyUsed?: boolean;
}