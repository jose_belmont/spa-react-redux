export interface FormValues {
    username: string;
    email: string;
    age: string;
}
