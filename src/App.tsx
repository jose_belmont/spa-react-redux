import React, { Component } from 'react';
import Menu from './components/Menu/index';
import Body from './components/Body/index';
import Footer from './components/Footer/index';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import './app-styles.scss';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import './assets/fonts/NimbusSansDOT-Bold.ttf';

export interface HelloProps {
  company: string;
  type: string;
}

class App extends Component {
  render() {
    return (
      <div>
        <Menu></Menu>
        <Body></Body>
        <Footer></Footer>
      </div>
    );
  }
}
export default App;
