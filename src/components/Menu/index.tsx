import React, { Component } from 'react';
import './menu-styles.scss';
import logo from '../../assets/finetwork_logo.png';
import { Button, Level } from 'react-bulma-components';

class Menu extends Component {

  private handleClick(): void {
    alert("Te estamos llamando");
  }

  render() {
    return (
      <div className="nav-bar">
        <Level renderAs="nav">
          <Level.Item textAlignment="centered">
            <img src={logo} alt="no found" />
          </Level.Item>
          <Level.Item textAlignment="centered">
            <span>Tarifas Movil</span>
          </Level.Item>
          <Level.Item textAlignment="centered">
            <span>tarifas Fibra</span>
          </Level.Item>

          <Level.Item textAlignment="centered">
            <span>Contactanos</span>
          </Level.Item>
          <Level.Item>
            <Button color="info" rounded onClick={this.handleClick}>
              <span>Te llamamos ?</span>
            </Button>
          </Level.Item>
        </Level>
      </div>
    );
  }
}

export default Menu;
