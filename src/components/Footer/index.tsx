import React, { Component } from 'react';
import './footer-styles.scss';
import { Level, Image } from 'react-bulma-components';
import instagram from '../../assets/icons/icon-instagram.svg';
import facebook from '../../assets/icons/icon-facebook.svg';
import apple from '../../assets/b_appstore_ok.png';
import googlePlay from '../../assets/b_gplay_ok.png';

class Footer extends Component {
  render() {
    return (
      <div className="app-footer">
        <Level>
          <Level.Item>
            <h3>Social Network: </h3>
            <Image className="soc-net-img" src={facebook}></Image>
            <Image className="soc-net-img" src={instagram}></Image>
          </Level.Item>
          <Level.Item textAlignment="centered">
            <Image className="mobile-app-img mob-app-marg" src={apple}></Image>
            <Image
              className="mobile-app-img mob-app-marg"
              src={googlePlay}
            ></Image>
          </Level.Item>
        </Level>
        <Level>
          <Level.Item textAlignment="centered">
            <h1>Ejercicio práctico</h1>
          </Level.Item>
        </Level>
      </div>
    );
  }
}

export default Footer;
