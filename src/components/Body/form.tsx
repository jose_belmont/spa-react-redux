import { Component } from 'react';
import React from 'react';
import showResults from './show-results';
import { Provider } from 'react-redux';
import SyncValidationForm from './form-validation';
import store from '../../store';

class Formulary extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="columns">
          <div className="column">
            <SyncValidationForm onSubmit={showResults} />
          </div>
        </div>
      </Provider>
    );
  }
}

export default Formulary;
