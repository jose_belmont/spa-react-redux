import React from 'react';
import { Field, reduxForm, isPristine } from 'redux-form';
import { FormValues } from '../../models/form-model';
import '../../../node_modules/bulma/css/bulma.min.css';
import './body-styles.scss';

function validate(values: FormValues): FormValues {
  const errors: FormValues = {
    username: '',
    email: '',
    age: ''
  };
  if (!values.username) {
    errors.username = 'Required';
  } else if (values.username.length > 15) {
    errors.username = 'Must be 15 characters or less';
  }
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.age) {
    errors.age = 'Required';
  } else if (isNaN(Number(values.age))) {
    errors.age = 'Must be a number';
  } else if (Number(values.age) < 18) {
    errors.age = 'Sorry, you must be at least 18 years old';
  }
  return errors;
}

function warn(values: FormValues): FormValues {
  const warnings: FormValues = {
    username: '',
    email: '',
    age: ''
  };

  if (Number(values.age) < 19) {
    warnings.age = 'Just 18';
  }
  return warnings;
}

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}: any) => (
  <div className="column">
    <div className="field">
      <label className="label">{label}</label>
    </div>
    <div>
      <input
        className={
          touched && error
            ? 'input is-danger'
            : touched && warning
            ? 'input is-warning'
            : touched || isPristine
            ? 'input is-success'
            : ''
        }
        {...input}
        placeholder={label}
        type={type}
      />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);

const SyncValidationForm = (props: {
  handleSubmit: any;
  pristine: any;
  reset: any;
  submitting: any;
}) => {
  const { handleSubmit, pristine, reset, submitting } = props;
  return (
    <form onSubmit={handleSubmit}>
      <div className="columns form-container">
        <Field
          name="username"
          type="text"
          component={renderField}
          label="Username"
        />
        <Field
          name="email"
          type="email"
          component={renderField}
          label="Email"
        />
        <Field name="age" type="number" component={renderField} label="Age" />
        <button
          className="button is-primary column btn-form"
          type="submit"
          disabled={submitting}
        >
          Submit
        </button>
        <button
          className="button is-info column btn-form"
          type="button"
          disabled={pristine || submitting}
          onClick={reset}
        >
          Reset
        </button>
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'syncValidation', 
  validate, 
  warn 
})(SyncValidationForm);
