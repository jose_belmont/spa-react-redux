import React, { Component } from 'react';
import './body-styles.scss';
import Slider from './slider';
import Formulary from './form';

class Body extends Component {
  render() {
    return (
      <div className="form-container">
        <Slider />
        <Formulary/>
      </div>
    );
  }
}

export default Body;
