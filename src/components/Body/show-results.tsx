import { FormValues } from '../../models/form-model';
import { FakeResponse } from '../../models/HttpResponse-model';

function sleep(ms: number) {
  new Promise(resolve => setTimeout(resolve, ms));
}

function simulateResponse(isResponseOk: boolean): FakeResponse {

  return {
    type: 'basic',
    url: 'http://localhost:3000/',
    redirected: false,
    status: isResponseOk ? 200 : 500,
    ok: isResponseOk ? true : false,
    statusText: isResponseOk ? 'OK' : 'KO',
    headers: {},
    body: undefined,
    bodyUsed: false
  };
}

export default (async function showResults(values: FormValues) {
  await sleep(500); // simulate server latency
  const date: Date = new Date();

  //Random result Simulate Result
  if (date.getMilliseconds() % 2 === 0) {
    const response: FakeResponse = simulateResponse(true);
    window.alert(
      `You submitted:\n\n${JSON.stringify(
        values,
        null,
        2
      )}\n\nThe response code is: ${response.status}`
    );
  } else {
    const response: FakeResponse = simulateResponse(false);
    window.alert(`Internal error: ${response.status}`);
  }
});
