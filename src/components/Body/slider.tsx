import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';
import img1 from '../../assets/banner-lifestyle-d.jpg';
import img2 from '../../assets/app-image-d.png';
import './body-styles.scss';

class Slider extends Component {
  render() {
    return (
      <Carousel
        showThumbs={false}
        dynamicHeight={true}
        showStatus={false}
        autoPlay={true}
        infiniteLoop={true}
        stopOnHover={true}
      >
        <div>
          <img className="carousel-img" src={img1} alt="img1" />
          <p className="legend">We are rich</p>
        </div>
        <div>
          <img className="carousel-img" src={img2} alt="img2" />
          <p className="legend">Join us</p>
        </div>
        <div>
          <img className="carousel-img" src={img1} alt="img3" />
          <p className="legend">This is for me</p>
        </div>
      </Carousel>
    );
  }
}

export default Slider;
