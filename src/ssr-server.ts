import express from 'express';
import next from 'next';
    
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
    
app.prepare()
.then(() => {
  const server = express()
    
  server.get('*', (req: any, res: any) => {
    return handle(req, res)
  })
    
  server.listen(3030, (err: any) => {
    if (err) throw err
    console.log('> Ready on http://localhost:3030')
  })
})
.catch((ex: any) => {
  console.error(ex.stack)
  process.exit(1)
})